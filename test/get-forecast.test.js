import { test } from 'tap';
import nock from 'nock';
import startApp from '../app.js';
import {
  responseGeolocationData,
  responseForecastWeatherByIpData,
  responseForecastWeatherData,
  responseIpApi,
} from './nock-responses.js';

test('GET `/forecast/:city`- Should succeed with location by ip', async (t) => {
  const app = startApp();

  nock(`${process.env.IP_API_URL}`)
    .persist()
    .get('/186.22.245.74')
    .reply(200, responseIpApi);

  nock(`${process.env.OPEN_WEATHER_API_URL_DATA}`)
    .persist()
    .get('/forecast')
    .query({
      lat: responseIpApi.lat,
      lon: responseIpApi.lon,
      appid: process.env.OPEN_WEATHER_API_KEY,
    })
    .reply(200, responseForecastWeatherByIpData);

  const weatherInfo = responseForecastWeatherByIpData.list.map((data) => ({
    weatherStatus: data.weather,
    date: data.dt_txt,
  }));

  t.teardown(() => app.close());
  const response = await app.inject({
    method: 'GET',
    url: 'api/v1/forecast/',
    headers: ({
      'Remote-Addr': '186.22.245.74',
      'X-Forwarded-For': '186.22.245.74',
    }),
  });
  t.equal(response.statusCode, 200);
  t.has(response.json().city, responseIpApi);
  t.has(response.json().weatherInfo, weatherInfo);
  nock.cleanAll();
  t.end();
});

test('GET `/forecast/:city`- Should fail with invalid city param', async (t) => {
  const app = startApp();

  const cityName = 'TestCity';

  nock(`${process.env.OPEN_WEATHER_API_URL_GEO}`)
    .persist()
    .get('/direct')
    .query({
      q: cityName,
      limit: 1,
      appid: process.env.OPEN_WEATHER_API_KEY,
    })
    .reply(200, []);

  t.teardown(() => app.close());
  const response = await app.inject({
    method: 'GET',
    url: `api/v1/forecast/${cityName}`,
  });
  t.equal(response.statusCode, 400);
  t.has(response.json(), {
    error: 'City Not Found',
    error_code: 400,
  });
  nock.cleanAll();
  t.end();
});

test('GET `/forecast/:city`- Should succeed with optional city param', async (t) => {
  const app = startApp();
  const cityName = 'Buenos Aires';

  nock(`${process.env.OPEN_WEATHER_API_URL_GEO}`)
    .persist()
    .get('/direct')
    .query({
      q: cityName,
      limit: 1,
      appid: process.env.OPEN_WEATHER_API_KEY,
    })
    .reply(200, responseGeolocationData);

  nock(`${process.env.OPEN_WEATHER_API_URL_DATA}`)
    .persist()
    .get('/forecast')
    .query({
      lat: responseGeolocationData[0].lat,
      lon: responseGeolocationData[0].lon,
      appid: process.env.OPEN_WEATHER_API_KEY,
    })
    .reply(200, responseForecastWeatherData);

  const weatherInfo = responseForecastWeatherData.list.map((data) => ({
    weatherStatus: data.weather,
    date: data.dt_txt,
  }));

  t.teardown(() => app.close());
  const response = await app.inject({
    method: 'GET',
    url: `api/v1/forecast/${cityName}`,
  });
  t.equal(response.statusCode, 200);
  t.has(response.json().city, responseGeolocationData[0]);
  t.has(response.json().weatherInfo, weatherInfo);
  nock.cleanAll();
  t.end();
});
