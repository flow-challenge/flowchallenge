import { test } from 'tap';
import nock from 'nock';
import startApp from '../app.js';
import { responseIpApi } from './nock-responses.js';

test('GET `/v1/location`- Should succeed with correct ip', async (t) => {
  const app = startApp();

  nock(`${process.env.IP_API_URL}`)
    .persist()
    .get('/186.22.245.74')
    .reply(200, responseIpApi);

  t.teardown(() => app.close());
  const response = await app.inject({
    method: 'GET',
    headers: ({
      'Remote-Addr': '186.22.245.74',
      'X-Forwarded-For': '186.22.245.74',
    }),
    url: 'api/v1/location',
  });
  t.equal(response.statusCode, 200);
  t.has(response.json(), responseIpApi);
  nock.cleanAll();
  t.end();
});

test('GET `/v1/location`- Should fail with reserved ip', async (t) => {
  const app = startApp();

  nock(`${process.env.IP_API_URL}`)
    .persist()
    .get('/127.0.0.1')
    .reply(500, {
      status: 'fail',
      message: 'reserved range',
      query: '127.0.0.1',
    });

  t.teardown(() => app.close());

  const response = await app.inject({
    method: 'GET',
    headers: ({
      'Remote-Addr': '127.0.0.1',
      'X-Forwarded-For': '127.0.0.1',
    }),
    url: 'api/v1/location',
  });
  t.equal(response.statusCode, 500);
  t.has(response.json(), {
    error: 'reserved range',
    error_code: 500,
  });
  nock.cleanAll();
  t.end();
});
