import startApp from '../app.js';

const app = await startApp();

app.listen(
  { port: process.env.API_PORT || 3000 },
  (error) => {
    if (error) {
      app.log.error(error);
      process.exit(1);
    }
  },
);
