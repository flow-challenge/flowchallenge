import locationRoute from './get-location.js';
import currentRoute from './get-current.js';
import forecastRoute from './get-forecast.js';

export default (fastify, opts, done) => {
  fastify.get('/location', locationRoute);
  fastify.get('/current/:city', currentRoute);
  fastify.get('/forecast/:city', forecastRoute);
  done();
};
