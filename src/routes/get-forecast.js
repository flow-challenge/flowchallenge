import {
  getLocation, getLocationOfCity, getForecastLocationWeather,
} from '../utils/general.js';

export default async function forecastRoute(request, reply) {
  let cityData;

  if (request.params.city) {
    cityData = await getLocationOfCity(reply, request.params.city);
  } else {
    cityData = await getLocation(reply, request.ip);
  }
  const result = await getForecastLocationWeather(reply, cityData.lat, cityData.lon);

  const weatherInfo = result.list.map((data) => ({
    weatherStatus: data.weather,
    date: data.dt_txt,
  }));

  return reply.code(200).send({
    city: cityData,
    weatherInfo,
  });
}
