import { getLocation } from '../utils/general.js';

export default async function locationRoute(request, reply) {
  const location = await getLocation(reply, request.ip);
  return reply.code(200).send(location);
}
