import {
  getLocation, getLocationOfCity, getCurrentLocationWeather,
} from '../utils/general.js';

export default async function currentRoute(request, reply) {
  let cityData;

  if (request.params && request.params.city) {
    cityData = await getLocationOfCity(reply, request.params.city);
  } else {
    cityData = await getLocation(reply, request.ip);
  }
  const result = await getCurrentLocationWeather(reply, cityData.lat, cityData.lon);
  return reply.code(200).send({
    city: cityData,
    weather: result.weather,
  });
}
