import fetch from 'node-fetch';

export async function getLocation(reply, ip) {
  return fetch(`${process.env.IP_API_URL}${ip}`)
    .then(async (response) => {
      const ipLocation = await response.json();
      if (!ipLocation) {
        return reply.code(500).send({
          error: 'Internal Error',
          error_code: 500,
        });
      }
      if (ipLocation.status === 'fail') {
        return reply.code(500).send({
          error: ipLocation.message,
          error_code: 500,
        });
      }
      return ipLocation;
    })
    .catch(() => reply.code(500).send({
      error: 'Internal Error',
      error_code: 500,
    }));
}

export async function getLocationOfCity(reply, cityName) {
  return fetch(`${process.env.OPEN_WEATHER_API_URL_GEO}direct?q=${cityName}&limit=1&appid=${process.env.OPEN_WEATHER_API_KEY}`)
    .then(async (response) => {
      const cityData = await response.json();
      if (!cityData || cityData.length === 0) {
        return reply.code(400).send({
          error: 'City Not Found',
          error_code: 400,
        });
      }
      return cityData[0];
    })
    .catch(() => reply.code(500).send({
      error: 'Internal Error',
      error_code: 500,
    }))
    .catch(() => reply.code(500).send({
      error: 'Internal Error',
      error_code: 500,
    }));
}

export async function getCurrentLocationWeather(reply, latitude, longitude) {
  return fetch(`${process.env.OPEN_WEATHER_API_URL_DATA}weather?lat=${latitude}&lon=${longitude}&appid=${process.env.OPEN_WEATHER_API_KEY}`)
    .then(async (response) => {
      const currentLocationWeatherData = await response.json();
      if (!currentLocationWeatherData) {
        return reply.code(500).send({
          error: 'Internal Error',
          error_code: 500,
        });
      }

      if (currentLocationWeatherData.cod === '400') {
        return reply.code(400).send({
          error: currentLocationWeatherData.message,
          error_code: 400,
        });
      }
      return currentLocationWeatherData;
    })
    .catch(() => reply.code(500).send({
      error: 'Internal Error',
      error_code: 500,
    }));
}

export async function getForecastLocationWeather(reply, latitude, longitude) {
  return fetch(`${process.env.OPEN_WEATHER_API_URL_DATA}forecast?lat=${latitude}&lon=${longitude}&appid=${process.env.OPEN_WEATHER_API_KEY}`)
    .then(async (response) => {
      const forecastWeatherData = await response.json();
      if (!forecastWeatherData) {
        return reply.code(500).send({
          error: 'Internal Error',
          error_code: 500,
        });
      }

      if (forecastWeatherData.cod === '400') {
        return reply.code(400).send({
          error: forecastWeatherData.message,
          error_code: 400,
        });
      }
      return forecastWeatherData;
    })
    .catch(() => reply.code(500).send({
      error: 'Internal Error',
      error_code: 500,
    }));
}
