import fastify from 'fastify';
import * as dotenv from 'dotenv';
import routes from './src/routes/index.js';

dotenv.config();

export default function startApp() {
  const app = fastify({
    logger: true,
    trustProxy: true,
  });
  app.register(routes, { prefix: 'api/v1' });

  return app;
}
