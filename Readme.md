# Servicios utilizados
A continuación listo las direcciones de los servicios que he utilizado en este proyecto:

- https://openweathermap.org/api
- https://ip-api.com/


Previo a intentar correr el proyecto se debe obtener una api key publica de `openweathermap.org`. Solo basta con registrarse en el sitio y validar su cuenta de correo.
Para registrarse, acceder a este [link](https://home.openweathermap.org/users/sign_in)


# Instrucciones del entorno

Para levantar localmente el proyecto se debe generar un archivo .env donde se guardarán las variables de entorno a utilizar. 
Dejo el archivo .env.dist para utilizarse de plantilla, simplemente hacer una copia y renombrar.

# Librerias utilizadas en el proyecto

Este proyecto utilizó las siguientes librerias:

- dotenv
- fastify
- node-fetch
- nodemon
- eslint
- tap

## Comandos del proyecto

Para poder correr el proyecto simplemente con usar `npm start`, teniendo el archivo .env creado, es suficiente.
Para el desarrollo se utilizó nodemon, para acceder utilizar el comando `npm run dev`.
Para correr los tests realizados con node-tap, correrlos con `npm test`.